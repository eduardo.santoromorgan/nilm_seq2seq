FROM tensorflow/tensorflow:latest-py3

RUN python --version

RUN apt-get update &&\
    pip install pandas matplotlib jupyter ipywidgets &&\
    pip uninstall ipykernel -y &&\
    pip install ipykernel blinker &&\
    pip install seaborn h5py numpy tqdm scikit-learn pyyaml simple-configuration &&\
    pip install invoke &&\
    mkdir -p /nilm_seq2seq

WORKDIR /nilm_seq2seq

COPY ./conf/.jupyter /root/.jupyter
COPY run_jupyter.sh /run_jupyter.sh

RUN chmod +x /run_jupyter.sh

EXPOSE 8888 6006

ENTRYPOINT ["/run_jupyter.sh"]
