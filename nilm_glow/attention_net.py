import tensorflow as tf


class ResidualBlock(tf.keras.Model):
    def __init__(self, n_filters, dilation_rate, name=''):
        super(ResidualBlock, self).__init__(name)
        self.n_filters = n_filters
        self.dilation_rate = dilation_rate
        self.tanh_out = tf.keras.layers.Conv1D(filters=n_filters,
                                               kernel_size=2,
                                               activation=tf.math.tanh,
                                               use_bias=True,
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               dilation_rate=dilation_rate,
                                               padding='same',
                                               )
        self.sigm_out = tf.keras.layers.Conv1D(filters=n_filters,
                                               kernel_size=2,
                                               dilation_rate=dilation_rate,
                                               activation=tf.math.sigmoid,
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               padding='same'
                                               )
        # self.mul_out = tf.multiply(tanh_out, sigm_out)

        self.multiply = tf.multiply

        self.residual_out = tf.keras.layers.Conv1D(filters=n_filters,
                                                   kernel_size=1,
                                                   padding='same',
                                                   use_bias=True,
                                                   kernel_regularizer=tf.keras.regularizers.l2(0.01))

        self.skip_out = tf.keras.layers.Conv1D(filters=n_filters,
                                               kernel_size=1,
                                               padding='same',
                                               use_bias=True,
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01)
                                               )
        self.add = tf.add

    def call(self, x, **kwargs):
        training = False if 'training' not in kwargs.keys() else kwargs['training']
        tanh_hout = self.tanh_out(x)
        sigm_out = self.sigm_out(x)

        mult = self.multiply(tanh_hout, sigm_out)

        residual_out = self.residual_out(mult)

        skip_out = self.skip_out(mult)

        residual_2_out = self.add(x, residual_out)
        return residual_2_out, skip_out


class AttentionNet(tf.keras.Model):
    def __init__(self, n_channels_out, n_stacks=1, dilation_depth=4, n_filters=32, use_skip_connections=True):
        super(AttentionNet, self).__init__()
        self.n_stacks = n_stacks
        self.n_filters = n_filters
        self.dilation_depth = dilation_depth
        self.use_skip_connections = use_skip_connections
        res_block = []
        self.conv1 = tf.keras.layers.Conv1D(kernel_size=2, filters=n_filters, padding='same')
        for s in range(n_stacks):
            res_block.append([])
            for d in range(dilation_depth + 1):
                res_block[s].append(ResidualBlock(dilation_rate=2 ** d, n_filters=n_filters, name='sd'))
        self.res_block = res_block
        self.conv_out = tf.keras.layers.Conv1D(filters=n_channels_out, kernel_size=1, padding='same',
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01))
        self.add_n = tf.add_n
        self.relu = tf.nn.relu
        # self.metrics = [tf.keras.metrics.MeanAbsoluteError(name='mae')]

    def call(self, x, **kwargs):
        x = tf.squeeze(x, axis=2)
        y = self.conv1(x)
        skip_connections = []
        for s in range(self.n_stacks):
            for d in range(self.dilation_depth + 1):
                y, skip_out = self.res_block[s][d](y)
                skip_connections += [skip_out]
        if self.use_skip_connections:
            y = self.add_n(skip_connections)
        y = self.relu(y)
        y = self.conv_out(y)
        y = tf.expand_dims(y, axis=2)
        shift = y[:, :, :, 0::2]
        scale = y[:, :, :, 1::2]
        return shift, scale