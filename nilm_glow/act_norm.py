import tensorflow as tf
import numpy as np


class ActNorm(tf.keras.layers.Layer):

    def __init__(self):
        super(ActNorm, self).__init__()

    def build(self, input_shape, **kwargs):
        x = kwargs['x']

        # scale_init = tf.nn.moments(x, axes=[0, 1, 2], keepdims=True)
        self.scale = self.add_weight(initializer='ones',
                                     shape=(1, 1, 1, x.shape[-1]),
                                     trainable=True,
                                     dtype=tf.float32
                                     )
        self.shift = self.add_weight(initializer='zeros',
                                     shape=(1, 1, 1, x.shape[-1]),
                                     trainable=True,
                                     dtype=tf.float32)
        self.initialized = self.add_weight(initializer='zeros',
                                           shape=(),
                                           trainable=False,
                                           dtype=tf.float32)

    def initialize_weights(self, x):
        shift_init = -1 * np.mean(x, axis=(0, 1, 2), keepdims=True)
        scale_init = np.log(1 / (1e-6 + np.std(x, axis=(0, 1, 2), keepdims=True)))
        self.set_weights([
            scale_init, shift_init, np.array(1.)
        ])

    def call(self, x, **kwargs):
        logdet = kwargs['logdet']
        reverse = kwargs['reverse']
        x_shape = x.shape
        if self.initialized == 0:
            self.initialize_weights(x)
            print('xxxp')
        if not reverse:
            logdet_factor = int(x_shape[1]) * int(x_shape[2])
            logs = tf.maximum(self.scale, 1e-10) * 1.0
            dlogdet = tf.reduce_sum(logs) * logdet_factor
            x += self.shift
            x_norm = x * tf.exp(logs)
            logdet += dlogdet
        else:
            logdet_factor = int(x_shape[1]) * int(x_shape[2])
            logs = tf.maximum(self.scale, 1e-10) * 1.0
            x = x * tf.exp(-logs)
            dlogdet = tf.reduce_sum(logs) * logdet_factor
            x_norm = x - self.shift
            logdet -= dlogdet
        return x_norm, logdet
