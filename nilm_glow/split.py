import tensorflow as tf


class Split(tf.keras.layers.Layer):

    def __init__(self):
        super(Split, self).__init__()

    def call(self, inputs, **kwargs):
        shape = inputs.shape
        y = inputs[:, :, :, : shape[-1] // 2]
        z = inputs[:, :, :, shape[-1] // 2:]
        return y, z
