import tensorflow as tf
from .attention_net import AttentionNet


class AffineCouplingLayer(tf.keras.layers.Layer):

    def __init__(self, alternating_flow=True, current_step=0):
        super(AffineCouplingLayer, self).__init__()
        self.alternating_flow = alternating_flow
        self.current_step = current_step

    def build(self, input_shape, **kwargs):
        attention_net_input_shape = tf.TensorShape(
            [input_shape[0], input_shape[1], input_shape[2], input_shape[3] // 2])
        self.attention_network = AttentionNet(n_channels_out=input_shape[-1])
        self.attention_network.build(attention_net_input_shape)
        self._trainable_weights = self.attention_network._trainable_weights

    def call(self, x, **kwargs):
        logdet = kwargs['logdet']
        reverse = kwargs['reverse']
        n_channels = x.shape[-1]
        xa = x[:, :, :, :n_channels // 2]
        xa_shape = xa.get_shape().as_list()
        if len(xa_shape) < 4:
            xa = tf.expand_dims(xa, -1)
        xb = x[:, :, :, n_channels // 2:]
        if self.alternating_flow:
            if self.current_step % 2 == 0:
                x_pass_through = xb
                x_op = xa
            else:
                x_pass_through = xa
                x_op = xb
        else:
            x_pass_through = xb
            x_op = xa
        if not reverse:
            # log_s, t = _nn(x=xb, name=name)
            # log_s, t = mini_unet(x=x_pass_through, name=name, training=training)
            log_s, t = self.attention_network(x_pass_through)
            # maybe needs to change to sigm
            s = tf.math.exp(log_s)
            ya = s * x_op + t
            yb = x_pass_through
            y = tf.concat([ya, yb], axis=-1)
            logdet += tf.reduce_sum(s, axis=[1, 2, 3])
        else:
            # log_s, t = _nn(x=xb, name=name)
            # log_s, t = mini_unet(x=x_pass_through, name=name, training=training)
            log_s, t = self.attention_network(x_pass_through)
            # maybe needs to change to sigm
            s = tf.math.exp(-log_s)
            ya = (x_op - t) * s
            yb = x_pass_through
            y = tf.concat([ya, yb], axis=-1)
            logdet -= tf.reduce_sum(s, axis=[1, 2, 3])
        return y, logdet
