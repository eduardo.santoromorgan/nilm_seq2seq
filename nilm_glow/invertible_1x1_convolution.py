import tensorflow as tf
import numpy as np


class Invertible1x1Convolution(tf.keras.layers.Layer):
    def __init__(self):
        super(Invertible1x1Convolution, self).__init__()

    def build(self, input_shape, **kwargs):
        w_shape = [input_shape[3], input_shape[3]]
        # w_init = np.linalg.qr(np.random.randn(*w_shape))[0]  # .astype('float32')
        self.w = self.add_weight(initializer='orthogonal',
                                 shape=w_shape,
                                 trainable=True,
                                 )

    def call(self, inputs,  **kwargs):
        logdet = kwargs['logdet']
        reverse = kwargs['reverse']
        w_shape = self.w.shape
        dlogdet = tf.math.log(tf.math.abs(tf.linalg.det(self.w))) * inputs.shape[1] * inputs.shape[2]
        # sign, log_abs_determinant = tf.linalg.slogdet(self.w)
        # dlogdet = sign * log_abs_determinant * inputs.shape[1] * inputs.shape[2]
        if not reverse:
            filters = tf.reshape(self.w, [1, 1] + w_shape)
            z = tf.nn.conv2d(inputs, filters, [1, 1, 1, 1], 'SAME', data_format='NHWC')
            logdet += dlogdet
            return z, logdet

        else:
            filters = tf.linalg.inv(self.w)
            filters = tf.reshape(filters, [1, 1] + w_shape)
            z = tf.nn.conv2d(inputs, filters, [1, 1, 1, 1], 'SAME', data_format='NHWC')
            logdet -= dlogdet
            return z, logdet