import tensorflow as tf


class Squeeze(tf.keras.layers.Layer):

    def __init__(self, factor=2):
        self.factor = factor
        super().__init__()

    def call(self, x, **kwargs):
        y = tf.reshape(x, [-1, x.shape[1] // self.factor, 1, self.factor * x.shape[2]])
        return y
