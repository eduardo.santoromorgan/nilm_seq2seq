import tensorflow as tf
import numpy as np
from metrics import F1Score, SignalAggregateError, NormalizedDisaggregationError
from .act_norm import ActNorm
from .affine_coupling_layer import AffineCouplingLayer
from .invertible_1x1_convolution import Invertible1x1Convolution
from .prior_net import PriorNet
from .squeeze import Squeeze
from .split import Split


class NilmGlow(tf.keras.Model):
    def __init__(self, flow_steps=8, version='default', learning_rate=1e-4, **kwargs):
        super(NilmGlow, self).__init__(**kwargs)
        self.optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.train_writer = tf.summary.create_file_writer(f"./logs/nilm_glow/{version}/train")
        self.eval_writer = tf.summary.create_file_writer(f"./logs/nilm_glow/{version}/validation")
        self.flow_steps = []
        self.squeezes = []
        self.splits = []
        for step in range(flow_steps):
            self.flow_steps.append([])
            self.squeezes.append(Squeeze())
            self.flow_steps[-1] = [
                ActNorm(),
                Invertible1x1Convolution(),
                AffineCouplingLayer(current_step=0)
            ]
            if step < flow_steps - 1:
                self.splits.append(Split())
        self.prior_net = PriorNet(n_filters_input=5, n_filters_output=8)
        self.appliance_list = [
            'p_I',
            'p_II',
            'dpc_I',
            'dpc_II',
            'ef_I',
            'ef_II',
            'mm_I',
            'mm_II'
        ]

    def build(self, input_shape, **kwargs):
        for idx, step in enumerate(self.flow_steps):
            self.squeezes[idx].build(input_shape=input_shape)
            input_shape = self.squeezes[idx].compute_output_shape(input_shape=input_shape)
            self._trainable_weights += self.squeezes[idx].trainable_weights
            for layer in step:
                layer.build(input_shape=input_shape, **kwargs)
                input_shape = layer.compute_output_shape(input_shape=input_shape)
                self._trainable_weights += layer.trainable_weights
            if idx < len(self.flow_steps) - 1:
                self.splits[idx].build(input_shape=input_shape)
                input_shape = self.splits[idx].compute_output_shape(input_shape=input_shape)
                self._trainable_weights += self.splits[idx].trainable_weights
        self.prior_net.build(input_shape=kwargs['y_shape'])
        self._trainable_weights += self.prior_net.trainable_weights
        self.built = True

    def call(self, inputs, **kwargs):
        if not self.built:
            self.build(input_shape=inputs.shape, **kwargs, x=inputs)
        reverse = kwargs['reverse']
        logdet = 0
        y = inputs
        # if in "reverse" mode, for inference reverses order of flow steps
        flow_steps = reversed(self.flow_steps) if reverse else self.flow_steps
        for flow_step in flow_steps:
            layers = flow_step if not reverse else reversed(flow_step)
            for layer in layers:
                y, logdet = layer.call(y, logdet=logdet, reverse=reverse)
        return y, logdet

    @staticmethod
    def build_metrics(on_ref, x_mean, x_std):
        f1_score = F1Score(targets_mean=x_mean,
                           targets_std=x_std,
                           on_ref=on_ref)
        signal_aggregate_error = SignalAggregateError(targets_mean=x_mean, targets_std=x_std)
        normalized_disaggregation_error = NormalizedDisaggregationError(targets_mean=x_mean,
                                                                        targets_std=x_std)
        metrics = {'f1_score': f1_score,
                   'sae': signal_aggregate_error,
                   'nde': normalized_disaggregation_error}
        return metrics

    @staticmethod
    def prepare_for_training(x, y, train_index, test_index, epochs=200, x_mask=200):
        # splits train and validation
        x_train, y_train = x[train_index], y[train_index]
        x_test, y_test = x[test_index], y[test_index]
        # normalizes aggregate data
        mean = np.mean(y_train, axis=(0, 1, 2), keepdims=True)
        std = np.std(y_train, axis=(0, 1, 2), keepdims=True)
        y_train = (y_train - mean) / std
        y_test = (y_test - mean) / std
        # normalizes appliance data
        on_ref = np.mean(x_train, axis=(0, 1, 2), keepdims=True)
        x_mean = np.mean(x_train, axis=(0, 1, 2), keepdims=True)
        x_std = np.mean(x_train, axis=(0, 1, 2), keepdims=True)
        x_train = (x_train - x_mean) / x_std
        x_test = (x_test - x_mean) / x_std
        metrics = NilmGlow.build_metrics(on_ref=on_ref, x_mean=x_mean, x_std=x_std)
        return x_train, y_train, x_test, y_test, metrics

    def priornet_loss(self, z, mean, log_std):
        log_ps = -0.5 * (
                np.log(2 * np.pi) +
                2. * log_std + (z - mean) ** 2 /
                tf.exp(2. * log_std)
        )
        return tf.reduce_sum(log_ps, [1, 2, 3])

    def decimal_to_bits(self, nll, x_shape):
        bits_x = (
            nll /
            (
                np.log(2.) * x_shape[1] *
                x_shape[2] * x_shape[3]
            )
        )
        return bits_x

    # @tf.function
    def train(self, x, y, train_index, test_index, epochs=200, x_mask=None):
        print("Preparing data...")
        x_train, y_train, x_test, y_test, metrics = \
            NilmGlow.prepare_for_training(x, y, train_index, test_index, epochs=epochs, x_mask=x_mask)
        print("Starting training...")
        losses = []
        prior_net_losses = []
        for epoch in range(epochs):
            train_ds = self.make_dataset(x_train, y_train)
            epoch_logdet = 0
            epoch_prior_net_loss = 0
            iters = 0
            for x_true, y_true in train_ds:
                with tf.GradientTape() as tape:
                    x_true, y_true = tf.cast(x_true, tf.float32), tf.cast(y_true, tf.float32)
                    z, logdet = self.call(inputs=x_true, reverse=False, y_shape=y_true.shape)
                    mean, log_std = self.prior_net(y_true)
                    prior_net_loss = - self.priornet_loss(z=z, mean=mean, log_std=log_std)
                    loss = (logdet + prior_net_loss)
                    # loss_bits = self.decimal_to_bits(loss, x_true.shape)
                gradients = tape.gradient(loss, self.trainable_variables)
                self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))
                logdet_avg = tf.reduce_mean(logdet, axis=0)
                prior_net_loss_avg = tf.reduce_mean(prior_net_loss, axis=0)
                epoch_logdet += logdet_avg
                epoch_prior_net_loss += prior_net_loss_avg
                iters += 1
            losses += [epoch_logdet / iters]
            prior_net_losses += [epoch_prior_net_loss / iters]
            with self.train_writer.as_default():
                tf.summary.scalar('logdet', losses[-1], step=epoch)
                tf.summary.scalar('prior_net_loss', prior_net_losses[-1], step=epoch)
            self.train_writer.flush()
            mape, eval_logdet, eval_nlogdet = self.test(x_test, y_test, epoch, metrics=None)
            print(
                f"Epoch: {epoch}, Logdet: {losses[-1]}, "
                f"PN loss: {prior_net_losses[-1]}; Eval_logdet: {eval_logdet}; MAPE: {mape}, "
                f"Eval n logdet: {eval_nlogdet}")

    def test(self, x_true, y_true, epoch, metrics=None):
        for x_true, y_true in self.make_dataset(x_true, y_true, batch_size=x_true.shape[0]):
            x_true, y_true = tf.cast(x_true, tf.float32), tf.cast(y_true, tf.float32)
            y_pred_test, logdet = self.call(inputs=x_true, reverse=False)
            # y_pred_test = y_pred_test[:, :, :, :y_true.shape[3]]
            # loss_y = tf.keras.losses.MeanSquaredError()(y_true, y_pred_test)
            mean, log_std = self.prior_net(y_true)
            z = mean
            # y_true_repeated = tf.tile(y_true, [1, 1, 1, 2])
            x_pred, nlogdet = self.call(inputs=z[:, :, :, 0::2], reverse=True)
            with self.eval_writer.as_default():
                logdet = tf.reduce_mean(logdet, axis=0)
                nlogdet = tf.reduce_mean(nlogdet, axis=0)
                tf.summary.scalar('logdet', logdet, step=epoch)
                tf.summary.scalar('nlogdet', nlogdet, step=epoch)
                # loss_x = tf.keras.losses.MeanSquaredError()(x_pred, x_true)
                mape = tf.keras.losses.MeanAbsolutePercentageError()(y_true=x_true, y_pred=x_pred)
                tf.summary.scalar('mae', mape, step=epoch)
                if metrics is not None:
                    for i, appliance in enumerate(self.appliance_list):
                        x_true_i, x_pred_i = tf.squeeze(x_true[:, :, :, i]), tf.squeeze(x_pred[:, :, :, i])
                        for name, metric in metrics.items():
                            metric.update_state(x_true_i, x_pred_i)
                            metric_val = metric.result()
                            tf.summary.scalar(f'{name}_{appliance}', metric_val, step=epoch)
                            metric.reset_states()
                self.eval_writer.flush()
            return mape, logdet, nlogdet

    def make_dataset(self, x, y, batch_size=128):
        ds = tf.data.Dataset.from_tensor_slices((x, y)) \
            .batch(batch_size=batch_size) \
            .shuffle(100)
        return ds

# @TODO IMPLEMENT EVALUATION USING THIS >>>>
## https: // www.tensorflow.org / guide / effective_tf2
