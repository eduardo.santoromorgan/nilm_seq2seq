import tensorflow as tf


class PriorNet(tf.keras.Model):
    def __init__(self, n_filters_input, n_filters_output, **kwargs):
        super(PriorNet, self).__init__(**kwargs)
        self.conv1 = tf.keras.layers.Conv1D(filters=n_filters_input,
                                            kernel_size=8,
                                            activation='relu',
                                            padding='same')
        self.conv2 = tf.keras.layers.Conv1D(filters=n_filters_output,
                                            kernel_size=8,
                                            activation='relu',
                                            padding='same')
        self.conv3 = tf.keras.layers.Conv1D(filters=2*n_filters_output,
                                            kernel_size=8,
                                            activation='relu',
                                            padding='same')

    def call(self, inputs):
        inputs = tf.squeeze(inputs)
        yi = self.conv1(inputs)
        yi = self.conv2(yi)
        yi = self.conv3(yi)
        yi = tf.expand_dims(yi, axis=2)
        mean = yi[:, :, :, 0::2]
        log_std = yi[:, :, :, 1::2]
        return mean, log_std
