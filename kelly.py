import tensorflow as tf


class Kelly(tf.keras.Model):

    def __init__(self, window_size):
        super(Kelly, self).__init__()
        filters = 8
        kernel_size = 4
        self.conv1 = tf.keras.layers.Conv1D(filters=filters,
                                            kernel_size=kernel_size,
                                            activation=None,
                                            use_bias=True,
                                            padding='same'
                                            )
        self.dense1 = tf.keras.layers.Dense(units=window_size * filters, activation='relu')
        self.dense2 = tf.keras.layers.Dense(units=128, activation='relu')
        self.dense3 = tf.keras.layers.Dense(units=window_size * filters, activation='relu')

        self.conv2 = tf.keras.layers.Conv1D(filters=1,
                                            kernel_size=kernel_size,
                                            padding='same')

    def call(self, x):
        xi = self.conv1(x)
        xi = tf.keras.layers.Flatten()(xi)
        xi = self.dense1(xi)
        xi = self.dense2(xi)
        xi = self.dense3(xi)
        xi = tf.reshape(xi, [-1, 1024, 8])
        xi = self.conv2(xi)
        return xi
