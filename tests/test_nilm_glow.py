from nilm_glow import Invertible1x1Convolution, ActNorm, NilmGlow, AffineCouplingLayer
import tensorflow as tf
import numpy as np


def test_invertible_1x1_conv():
    conv = Invertible1x1Convolution()
    x = np.random.random_sample((1, 512, 1, 4))
    y, logdet = conv(x, reverse=False, logdet=0)
    x_p, logdet = conv(y, logdet=0, reverse=True)
    assert len(conv.trainable_variables) == 1, "Duplicating weights variable for invertible conv layer"
    # assert np.allclose(x_p, x), f"Conv 1x1 is not invertible error {np.mean(np.abs(x_p - x))}"


def test_act_norm():
    act_norm = ActNorm()
    x = np.random.random_sample((1, 512, 1, 4))
    y, logdet = act_norm(x, reverse=False, logdet=0)
    assert len(act_norm.trainable_variables) == 2, "Wrong number of trainable variables"
    x_p, logdet = act_norm(y, logdet=0, reverse=True)
    assert len(act_norm.trainable_variables) == 2, "Wrong number of trainable variables"
    assert np.allclose(x_p, x), "Act norm is not inversible"


def test_affine_coupling_layer():
    acl = AffineCouplingLayer()
    x = np.random.random_sample((1, 512, 1, 8))
    y, logdet = acl(x, reverse=False, logdet=0)
    trainable_variables_len = len(acl.trainable_variables)
    assert len(acl.trainable_variables) > 0, "Wrong number of trainable variables"
    x_p, logdet = acl(y, logdet=0, reverse=True)
    print(len(acl.trainable_variables))
    assert len(acl.trainable_variables) == trainable_variables_len, "Wrong number of trainable variables"
    assert np.allclose(x_p, x), "Affine coupling layer is not invertible"
    acl_odd = AffineCouplingLayer(current_step=1)
    x = np.random.random_sample((1, 512, 1, 4))
    y, logdet = acl_odd(x, logdet=0, reverse=False)
    x_p, logdet = acl_odd(y, logdet=0, reverse=True)
    # assert np.allclose(x_p, x), f"Affine coupling layer is not invertible when odd error: {np.mean(np.abs(x_p-x))}"


def test_nilm_glow():
    x = np.random.random_sample((1, 512, 1, 4))
    nilm_glow = NilmGlow(flow_steps=4)
    y, logdet = nilm_glow(x, reverse=False)
    weights = nilm_glow.trainable_variables
    x_p, logdet_2 = nilm_glow(y, reverse=True)
    for a, b in zip(weights, nilm_glow.trainable_variables):
        assert np.allclose(a.numpy(), b.numpy()), "Some variable is wrong"
    print(np.mean(np.abs(x_p - x)))


if __name__ == "__main__":
    test_invertible_1x1_conv()
    test_act_norm()
    test_affine_coupling_layer()
    test_nilm_glow()