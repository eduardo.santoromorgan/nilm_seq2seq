import tensorflow as tf
import numpy as np
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from nilm_glow import NilmGlow
from tests import test_act_norm, test_affine_coupling_layer, test_invertible_1x1_conv, test_nilm_glow
np.random.seed(42)
tf.random.set_seed(42)

dataset = np.load('./data/train_pifpaf_v1.npy')
n_windows = dataset.shape[0] // 1024

dataset = np.reshape(dataset[-1024 * n_windows:, :], [n_windows, 1024, 1, 13])

x, y = dataset[:, :, :, :8], dataset[:, :, :, 8:]

kfold = KFold(n_splits=2)

for fold, (train_index, test_index) in enumerate(kfold.split(x, y)):
    print(f'Fold: {fold}')
    nilm_glow = NilmGlow(version=f'iter_{fold}')
    nilm_glow.train(x, y, train_index, test_index)
    nilm_glow.save(f'./models/nilm_glow/fold-{fold}')
