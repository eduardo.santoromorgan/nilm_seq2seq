from sklearn.preprocessing import LabelEncoder
from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, Dropout, Convolution1D
from keras.utils import np_utils
from keras.callbacks import EarlyStopping
from collections import OrderedDict

import pandas as pd
import numpy as np
import datetime
from sklearn.model_selection import KFold

from rnn_models.models import convNet, kezhNet, wavenilm, jackKellysCnn
from library.extra import *
from library.metrics import events_detector
from library.sql_loader import get_attributes_from_meters
from nilmtk import DataSet
from nilmtk.measurement import LEVEL_NAMES
import matplotlib.pyplot as plt
import os
from library.metrics import MAE, F_score, NDE, SAE, MSE, NDE_backend
from keras import backend as K
from copy import deepcopy

import errno
import os


def save_scores_to_file(log_folder, appliances, f1_Score, nde, sae, mse, mae):
    text_file = open(log_folder + '/log.txt', "a+")
    text_file.write('=================Scores on Test Set================\r\n')
    text_file.write(
        'Mean and Standard Deviation with respect to a division on train/validation of KFold. (Test set is fixed for every KFold, but changes on appliance due to different measurement times)\r\n')
    for appliance in appliances:
        text_file.write('==========%s==========\r\n' % appliance)
        text_file.write(
            'F1 Score: mean:%2.6f, std:%2.6f\r\n' % (np.mean(f1_Score[appliance]), np.std(f1_Score[appliance])))
        text_file.write('NDE: mean:%2.6f, std:%2.6f\r\n' % (np.mean(nde[appliance]), np.std(nde[appliance])))
        text_file.write('SAE: mean:%2.6f, std:%2.6f\r\n' % (np.mean(sae[appliance]), np.std(sae[appliance])))
        text_file.write('MSE: mean:%2.6f, std:%2.6f\r\n' % (np.mean(mse[appliance]), np.std(mse[appliance])))
        text_file.write('MAE: mean:%2.6f, std:%2.6f\r\n' % (np.mean(mae[appliance]), np.std(mae[appliance])))
        text_file.write('====================\r\n')
    text_file.write('=================ALL Scores on Test Set================\r\n')
    for appliance in appliances:
        text_file.write('==========%s==========\r\n' % appliance)
        for metrics, metric_name in [(f1_Score, 'F1 Score'), (nde, 'NDE'), (sae, 'SAE'), (mse, 'MSE'), (mae, 'MAE')]:
            text_file.write('------%s------' % metric_name)
            for i, value in enumerate(metrics[appliance]):
                text_file.write('      Fold %i:%2.6f\r\n' % (i, value))
    text_file.close()
    return None


# ============================================================
# ====================Parameters==============================
# ============================================================
# dataset_filepath = os.path.join('/home', 'bandeira', 'Dados', 'PifPafMedidores.h5')
dataset_filepath = '/home/bandeira/Dataset/PifPaf/PifPafMedidores.h5'
metadata_filepath = '../metadata'
today = datetime.datetime.now().strftime('%Y-%m-%d-%H')
log_folder = '../logs/Cross-Validation/WaveNILM-' + today
mkdir_p(log_folder)

window_size = 1024
number_of_epochs = 5
number_of_splits = 2
sets_percentages = {'train': 0.7, 'validation': 0.15, 'test': 0.15}
site_meter_features = [('voltage', ''), ('current', '')]
appliance_features = [('power', 'active')]
number_of_features = len(site_meter_features)
devices = get_devices(metadata_filepath)
appliances = devices[10:]  # mudar
# site_meter = 'Quadro geral da Fabrica TC do TC'
site_meter = devices[0]
loss_function = 'mae'
optimization = 'nadam'
fig_format = 'eps'
# earlystop = EarlyStopping(monitor = 'val_loss', min_delta = 5, patience = 10, verbose = 1, mode = 'auto')

# ============================================================
# ====================Model Training==========================
# ============================================================
write_params_to_file(log_folder,
                     window_size,
                     number_of_epochs,
                     sets_percentages,
                     site_meter,
                     site_meter_features,
                     appliances,
                     appliance_features,
                     loss_function,
                     optimization,
                     number_of_splits)

f1_Score = OrderedDict()
nde = OrderedDict()
sae = OrderedDict()
mse = OrderedDict()
mae = OrderedDict()
model = {}
panel_dict = OrderedDict()

print('----Loading Dataset---')
dataset = DataSet(dataset_filepath)
for device in devices:
    panel_dict[device] = dataset.buildings[1].elec[devices.index(device) + 1].load(
    ).next().drop([('energy', 'reactive')], axis=1)

for appliance in appliances:
    print('========================== %s ==========================' % appliance)

    site_meter_df = deepcopy(panel_dict[site_meter][site_meter_features])
    appliance_df = deepcopy(panel_dict[appliance][appliance_features])
    merged_df = site_meter_df.merge(appliance_df,
                                    how='inner',
                                    left_index=True,
                                    right_index=True).dropna()
    columns_names = [np.array([site_meter] * number_of_features + [appliance] * len(appliance_features)),
                     np.array([site_meter_features[k][0] for k in range(number_of_features)] + \
                              [appliance_features[k][0] for k in range(len(appliance_features))]),
                     np.array([site_meter_features[k][1] for k in range(number_of_features)] + \
                              [appliance_features[k][1] for k in range(len(appliance_features))])
                     ]
    merged_df.columns = columns_names
    merged_df.columns.set_names(['meter'] + LEVEL_NAMES, inplace=True)

    # Creates folds of train and validation sets. It fixes test set.
    print('-----Using KFolds------')
    kf = KFold(n_splits=number_of_splits, shuffle=False, random_state=42)

    folds = kf.split(merged_df[:int(merged_df.shape[0] * (sets_percentages['train'] +
                                                          sets_percentages['validation']))])
    # app_dict = {k:panel_dict[k] for k in [site_meter, appliance] if k in panel_dict}
    # allSet = pd.Panel(app_dict).drop(('energy', 'active'), axis = 2).dropna(axis = 1)
    # trainingSet = deepcopy(allSet.ix[:, 0:int(allSet.major_axis.size*sets_percentages['train'])])
    # validationSet = deepcopy(allSet.ix[:, int(allSet.major_axis.size*sets_percentages['train']):int(allSet.major_axis.size*(sets_percentages['train']+sets_percentages['validation']))])
    # testSet = deepcopy(allSet.ix[:, int(allSet.major_axis.size*(sets_percentages['train']+sets_percentages['validation'])):])
    # validationX = validationSet[site_meter][[('voltage', ''), ('current', '')]].values
    # trainX  = trainingSet[site_meter][[('voltage', ''), ('current', '')]].values

    f1_Score[appliance] = []
    nde[appliance] = []
    sae[appliance] = []
    mse[appliance] = []
    mae[appliance] = []

    for i, fold in enumerate(folds):
        print('==Fold number %i of %i==' % (i + 1, number_of_splits))
        write_timestamps_to_file(log_folder, appliance, i, merged_df, sets_percentages, fold)

        ## Site Meter preprocessing
        validationX = deepcopy(merged_df[site_meter].iloc[fold[1]].values)
        trainX = deepcopy(merged_df[site_meter].iloc[fold[0]].values)
        train_means = []
        train_std = []
        train_mins = []
        for i in xrange(trainX.shape[1]):
            train_means.append(trainX[:, i].mean())
            train_std.append(trainX[:, i].std())
            train_mins.append(trainX[:, i].min())
            trainX[:, i] = (trainX[:, i] - train_means[i]) / train_std[i]
            validationX[:, i] = (validationX[:, i] - train_means[i]) / train_std[i]

        trainX = np.reshape(trainX[:int(trainX.shape[0] / window_size) * window_size],
                            (-1, window_size, number_of_features))
        validationX = np.reshape(validationX[:int(validationX.shape[0] / window_size) * window_size],
                                 (-1, window_size, number_of_features))

        ## Targets preprocessing
        trainY = deepcopy(merged_df[appliance].iloc[fold[0]].values)
        validationY = deepcopy(merged_df[appliance].iloc[fold[1]].values)
        trainY = np.reshape(trainY[:int(trainY.shape[0] / window_size) * window_size],
                            (-1, window_size, 1))
        validationY = np.reshape(validationY[:int(validationY.shape[0] / window_size) * window_size],
                                 (-1, window_size, 1))

        ## Modeling

        print('-----Loading Model-----')
        loading_path = '../models/base_model_for_transfer_learning/Moinho I-WaveNILM-2018-05-14'
        #        loading_path = '../models/base_model_for_transfer_learning/modelWaveNILM'
        model[appliance] = model_load(loading_path)
        # model[appliance] = load_model('../models/Peletizadora I-WaveNILM-Peletizadora-2018-01-25.h5')
        # model[appliance] = wavenilm((trainX.shape[1], trainX.shape[2]),
        #                            n_outputs = 1, 
        #                            n_filters = 32,
        #                            n_stacks = 5,
        #                            dilation_depth = 4)
        # model[appliance] = convNet((trainX.shape[1], trainX.shape[2]), binary = False)
        # model[appliance] = jackKellysCnn((trainX.shape[1], trainX.shape[2]))
        # for layer in model[appliance].layers[:-3]:
        #    layer.trainable = False
        model[appliance].compile(loss=loss_function, optimizer=optimization)

        history = model[appliance].fit(trainX, trainY,
                                       epochs=number_of_epochs, batch_size=32,
                                       validation_data=(validationX, validationY),
                                       verbose=2,
                                       # callbacks = [earlystop],
                                       shuffle=True)

        ## Testing
        print('-----Testing-----')
        testX = deepcopy(merged_df[site_meter][int(merged_df.shape[0] * (sets_percentages['train'] +
                                                                         sets_percentages['validation'])):].values)
        for i in xrange(testX.shape[1]):
            testX[:, i] = (testX[:, i] - train_means[i]) / train_std[i]
        windowed_test = np.reshape(testX[:int(testX.shape[0] / window_size) * window_size],
                                   (-1, window_size, number_of_features))

        predictedTest = np.reshape(model[appliance].predict(windowed_test), (-1, 1))

        testY = merged_df[appliance][int(merged_df.shape[0] * (sets_percentages['train'] +
                                                               sets_percentages['validation'])):].values

        f1_Score[appliance] += [
            F_score(events_detector(testY)[:predictedTest.shape[0]], events_detector(predictedTest)) * 100]
        nde[appliance] += [NDE(testY[:predictedTest.shape[0]], predictedTest)]
        sae[appliance] += [SAE(testY[:predictedTest.shape[0]], predictedTest)]
        mse[appliance] += [MSE(testY[:predictedTest.shape[0]], predictedTest)]
        mae[appliance] += [MAE(testY[:predictedTest.shape[0]], predictedTest)]

    plt.clf()
    plt.plot(history.history['loss'], label='training')
    plt.plot(history.history['val_loss'], label='validation')
    plt.title(appliance)
    plt.legend()
    plt.savefig(log_folder + '/loss_' + appliance + '.' + fig_format, format=fig_format)
    # try:
    #    plt.show(block = False)
    # except:
    #    print('X11 connection broke: I/O error')
    #    pass

    plt.clf()
    plt.plot(testY[35000:70000, 0], label='test')
    plt.plot(predictedTest[35000:70000, 0], label='predicted')
    plt.title(appliance)
    plt.legend()
    plt.savefig(log_folder + '/' + appliance + '.' + fig_format, format=fig_format)
    # try:
    #    plt.show(block = False)
    # except:
    #    print('X11 connection broke: I/O error')
    #    pass

save_scores_to_file(log_folder, appliances, f1_Score, nde, sae, mse, mae)

print('===================Scores========================')
for measure in ['f1_Score', 'nde', 'sae', 'mse', 'mae']:
    print('============== Test %s ==============' % measure)
    for app, score in vars()[measure].iteritems():
        print('%s:  mean:%2.6f, std:%2.6f' % (app, np.mean(score), np.std(score)))

mkdir_p(log_folder + '/models')
for appliance in model.iterkeys():
    saving_path = log_folder + '/models/' + appliance + '-WaveNILM-' + datetime.datetime.now().strftime('%Y-%m-%d')
    # model[appliance].save(saving_path)
    model_save(model[appliance], saving_path)
    print('Model %s saved on %s' % (appliance, saving_path))
