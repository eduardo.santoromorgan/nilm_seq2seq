import tensorflow as tf


class SignalAggregateError(tf.keras.metrics.Metric):
    def __init__(self, targets_mean, targets_std, **kwargs):
        super(SignalAggregateError, self).__init__(name='signal_aggregate_error', **kwargs)
        self.targets_mean = targets_mean
        self.targets_std = targets_std
        self.predicted_consumption = self.add_weight(name='predicted_consumption', initializer='zeros')#, dtype=tf.float64)
        self.target_consumption = self.add_weight(name='target_consumption', initializer='zeros')#, dtype=tf.float64)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred * self.targets_std + self.targets_mean
        y_true = y_true * self.targets_std + self.targets_mean

        self.predicted_consumption.assign_add(tf.cast(tf.reduce_sum(y_pred), tf.float32))
        self.target_consumption.assign_add(tf.cast(tf.reduce_sum(y_true), tf.float32))

    def result(self):
        return tf.abs(self.predicted_consumption - self.target_consumption) / self.target_consumption

    def reset_states(self):
        self.predicted_consumption.assign(0.)
        self.target_consumption.assign(0.)


class F1Score(tf.keras.metrics.Metric):
    def __init__(self, targets_mean, targets_std, on_ref=None, **kwargs):
        super(F1Score, self).__init__(name='f1_score', **kwargs)
        self.targets_mean = targets_mean
        self.targets_std = targets_std
        self.on_ref = on_ref
        self.true_positives = self.add_weight(name='true_positives', initializer='zeros')
        self.false_positives = self.add_weight(name='false_positives', initializer='zeros')
        self.true_negatives = self.add_weight(name='true_negatives', initializer='zeros')
        self.false_negatives = self.add_weight(name='false_negatives', initializer='zeros')

    def count_activations(self, signal):
        is_on = signal > self.on_ref
        return tf.cast(is_on, tf.float32)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred * self.targets_std + self.targets_mean
        y_true = y_true * self.targets_std + self.targets_mean

        y_true = self.count_activations(y_true)
        y_pred = self.count_activations(y_pred)

        true_positives = tf.reduce_sum(tf.cast(tf.logical_and(y_pred == 1, y_true == 1), tf.float32))
        true_negatives = tf.reduce_sum(tf.cast(tf.logical_and(y_pred == 0, y_true == 0), tf.float32))
        false_positives = tf.reduce_sum(tf.cast(tf.logical_and(y_pred == 1, y_true == 0), tf.float32))
        false_negatives = tf.reduce_sum(tf.cast(tf.logical_and(y_pred == 0, y_true == 1), tf.float32))

        self.true_positives.assign_add(true_positives)
        self.true_negatives.assign_add(true_negatives)
        self.false_positives.assign_add(false_positives)
        self.false_negatives.assign_add(false_negatives)

    def result(self):
        precision = self.true_positives / (self.true_positives + self.false_positives)
        recall = self.true_positives / (self.true_positives, self.false_negatives)

        return 2 * precision * recall / (precision + recall)

    def reset_states(self):
        self.true_positives.assign(0.)
        self.true_negatives.assign(0.)
        self.false_positives.assign(0.)
        self.false_negatives.assign(0.)


class NormalizedDisaggregationError(tf.keras.metrics.Metric):
    def __init__(self, targets_mean, targets_std, **kwargs):
        super(NormalizedDisaggregationError, self).__init__(name='nde', **kwargs)
        self.targets_mean = targets_mean
        self.targets_std = targets_std
        self.error = self.add_weight(name='error', initializer='zeros')
        self.targets = self.add_weight(name='targets', initializer='zeros')

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred * self.targets_std + self.targets_mean
        y_true = y_true * self.targets_std + self.targets_mean

        y_true = tf.expand_dims(y_true, -1)
        self.error.assign_add(tf.cast(tf.reduce_sum(tf.cast(y_true, tf.float32) - y_pred) ** 2, tf.float32))
        self.targets.assign_add(tf.cast(tf.reduce_sum(y_true) ** 2, tf.float32))

    def result(self):
        return self.error / self.targets

    def reset_states(self):
        self.error.assign(0.)
        self.targets.assign(0.)


