from wavenet import Wavenet
import tensorflow as tf
import numpy as np
from sklearn.model_selection import KFold
from metrics import F1Score, SignalAggregateError, NormalizedDisaggregationError
from scipy.signal import decimate
import os

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
# os.environ["CUDA_VISIBLE_DEVICES"] = "1"
# Use tensorcores
# enable tf mixed precision
os.environ['TF_ENABLE_AUTO_MIXED_PRECISION'] = '1'

kfold = KFold(7)
metrics = []
models = []
window_size = 1024

appliances = [#'pelletizer_I',
'pelletizer_II', 'double_pole_contactor_I', 'double_pole_contactor_II',
              'exhaust_fan_I', 'exhaust_fan_II', 'milling_machine_I', 'milling_machine_II']

for appliance_name in appliances:
    dataset = np.load(f'./data/{appliance_name}.npy')
    # dataset = decimate(dataset, 6, axis=0)
    if len(dataset.shape) == 2:
        print("xxx")
        padding = window_size - dataset.shape[0] % window_size
        dataset = np.pad(dataset, ((0, padding), (0, 0)), mode='constant')
        dataset = np.reshape(dataset, (-1, window_size, dataset.shape[1]))
    aggregate = dataset[:, :, :4]
    appliance = dataset[:, :, -1]
    for i, (train_index, test_index) in enumerate(kfold.split(aggregate)):
        wavenet = Wavenet()
        features, targets = aggregate[train_index], appliance[train_index]
        eval_features, eval_targets = aggregate[test_index], appliance[test_index]
        #
        # targets_mean = np.mean(targets, axis=(0, 1), keepdims=True)
        # targets = targets / targets_mean
        # eval_targets = eval_targets / targets_mean

        on_ref = np.mean(targets)

        # normalizes targets
        # targets_mean = np.mean(targets, keepdims=True)
        # targets_std = np.std(targets, keepdims=True)
        # targets = (targets - targets_mean) / targets_std
        targets = np.maximum(targets, np.zeros(targets.shape))
        eval_targets = np.maximum(eval_targets, np.zeros(eval_targets.shape))
        # eval_targets = (eval_targets - targets_mean) / targets_std
        # normalizes features channels so that they are all in similar scales
        features_mean = np.mean(features, axis=(0, 1), keepdims=True)
        features_std = np.std(features, axis=(0, 1), keepdims=True)
        features = (features - features_mean) / features_std
        eval_features = (eval_features - features_mean) / features_std

        os.system(f'mkdir ./trained_models/wavenet/{appliance_name}_{i}')
        csv_logger = tf.keras.callbacks.CSVLogger(f'./trained_models/wavenet/{appliance_name}_{i}/training.log')
        #
        reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.2,
                                                         patience=2, min_lr=0.001, min_delta=10)

        saver = tf.keras.callbacks.ModelCheckpoint(
            filepath=f'./trained_models/wavenet/{appliance_name}_{i}/checkpoints',
            monitor='val_signal_aggregate_error',
            save_best_only=True)

        tensorboard = tf.keras.callbacks.TensorBoard(log_dir=f'./trained_models/wavenet/{appliance_name}_{i}/logs/',
                                                     write_graph=i == 0,
                                                     update_freq='epoch',
                                                     write_images=i == 0)

        wavenet.compile(optimizer=tf.keras.optimizers.Adam(0.01),  # Optimizer
                        # Loss function to minimize
                        loss=tf.keras.losses.MeanSquaredError(),
                        # List of metrics to monitor
                        metrics=[tf.keras.metrics.MeanSquaredError(name='val_loss'),
                                 # F1Score(targets_mean=targets_mean, targets_std=targets_std, on_ref=on_ref),
                                 SignalAggregateError(targets_mean=0, targets_std=1),
                                 NormalizedDisaggregationError(targets_mean=0, targets_std=1),
                                 tf.keras.metrics.MeanAbsoluteError()])
        wavenet.fit(x=features,
                    y=targets,
                    validation_data=(eval_features, eval_targets),
                    epochs=200,
                    callbacks=[csv_logger, saver, tensorboard])
