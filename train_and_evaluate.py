import pandas as pd
from wavenet import WaveNet

appliances = []
for appliance in appliances:
    appliance_data = pd.DataFrame(f"./data/{appliance}.csv")
    model = WaveNet()
    metrics = model.train(appliance_data)
    metrics.to_csv(f"./metrics/{appliance}.csv")

