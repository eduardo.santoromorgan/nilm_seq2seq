from kelly import Kelly
import tensorflow as tf
import numpy as np
from sklearn.model_selection import KFold
from metrics import F1Score, SignalAggregateError, NormalizedDisaggregationError
from scipy.signal import decimate
import os
import math

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
# Use tensorcores
# enable tf mixed precision
os.environ['TF_ENABLE_AUTO_MIXED_PRECISION'] = '1'

kfold = KFold(5, random_state=0)
metrics = []
models = []
window_size = 1024

appliances = ['pelletizer_I', 'pelletizer_II', 'double_pole_contactor_I', 'double_pole_contactor_II',
              'exhaust_fan_I', 'exhaust_fan_II', 'milling_machine_I', 'milling_machine_II']

for appliance_name in appliances:
    dataset = np.load(f'./data/{appliance_name}.npy')
    # dataset = decimate(dataset, 6, axis=0)
    if len(dataset.shape) == 2:
        padding = window_size - dataset.shape[0] % window_size
        dataset = np.pad(dataset, ((0, padding), (0, 0)), mode='constant')
        dataset = np.reshape(dataset, (-1, window_size, dataset.shape[1]))
    aggregate = dataset[:, :, :4]
    # aggregate = np.expand_dims(np.sqrt(aggregate[:, :, 0] ** 2 + aggregate[:, :, 1] ** 2), axis=-1)
    appliance = np.expand_dims(dataset[:, :, -1], axis=-1)
    for i, (train_index, test_index) in enumerate(kfold.split(aggregate)):
        # kelly = Kelly(window_size=1024)
        features, targets = aggregate[train_index], appliance[train_index]
        eval_features, eval_targets = aggregate[test_index], appliance[test_index]

        # computes "ON"reference by estimating avg
        on_ref = np.mean(targets)

        # normalizes targets
        targets_mean = np.mean(targets, keepdims=True)
        targets_std = np.std(targets, keepdims=True)
        targets = (targets - targets_mean) / targets_std
        eval_targets = (eval_targets - targets_mean) / targets_std
        # normalizes features channels so that they are all in similar scales
        features_mean = np.mean(features, axis=(0, 1), keepdims=True)
        features_std = np.std(features, axis=(0, 1), keepdims=True)
        features = (features - features_mean) / features_std
        eval_features = (eval_features - features_mean) / features_std

        os.system(f'mkdir ./trained_models/kelly/{appliance_name}_{i}')
        csv_logger = tf.keras.callbacks.CSVLogger(f'./trained_models/kelly/{appliance_name}_{i}/training.log')
        #
        reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.2,
                                                         patience=2, min_lr=0.001, min_delta=10)

        saver = tf.keras.callbacks.ModelCheckpoint(filepath=f'./trained_models/kelly/{appliance_name}_{i}/checkpoints',
                                                   monitor='val_signal_aggregate_error',
                                                   save_best_only=True)

        tensorboard = tf.keras.callbacks.TensorBoard(log_dir=f'./trained_models/kelly/{appliance_name}_{i}/logs/',
                                                     write_graph=i == 0,
                                                     update_freq='epoch',
                                                     write_images=i == 0)
        filters = 8
        kernel_size = 4
        kelly = tf.keras.Sequential()
        kelly.add(tf.keras.layers.Conv1D(filters=filters,
                                         kernel_size=kernel_size,
                                         activation=None,
                                         use_bias=True,
                                         padding='same'
                                         ))
        kelly.add(tf.keras.layers.Flatten())
        kelly.add(tf.keras.layers.Dense(units=1024 * 8, activation=tf.keras.activations.relu))
        kelly.add(tf.keras.layers.Dense(units=128, activation=tf.keras.activations.relu))
        kelly.add(tf.keras.layers.Dense(units=1024 * 8, activation=tf.keras.activations.relu))
        kelly.add(tf.keras.layers.Reshape((1024, 8)))
        kelly.add(tf.keras.layers.Conv1D(filters=1, kernel_size=8, activation=None, use_bias=True, padding='same'))
        kelly.compile(optimizer=tf.keras.optimizers.Adam(0.001),  # Optimizer
                      # Loss function to minimize
                      loss=tf.keras.losses.MeanSquaredError(),
                      # List of metrics to monitor
                      metrics=[tf.keras.metrics.MeanSquaredError(name='val_loss'),
                               F1Score(targets_mean=targets_mean, targets_std=targets_std, on_ref=on_ref),
                               SignalAggregateError(targets_mean=targets_mean, targets_std=targets_std),
                               NormalizedDisaggregationError(targets_mean=targets_mean, targets_std=targets_std),
                               tf.keras.metrics.MeanAbsoluteError()])
        kelly.fit(x=features,
                  y=targets,
                  validation_data=(eval_features, eval_targets),
                  epochs=300,
                  batch_size=256,
                  callbacks=[csv_logger, saver, tensorboard])
