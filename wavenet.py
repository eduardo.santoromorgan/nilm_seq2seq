import tensorflow as tf


class ResidualBlock(tf.keras.layers.Layer):
    def __init__(self, n_filters, dilation_rate, name=''):
        super(ResidualBlock, self).__init__(name)
        self.n_filters = n_filters
        self.dilation_rate = dilation_rate

    def build(self, input_shape):
        self.tanh_out = tf.keras.layers.Conv1D(filters=self.n_filters,
                                               kernel_size=2,
                                               activation='tanh',
                                               use_bias=True,
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               dilation_rate=self.dilation_rate,
                                               padding='causal',
                                               name=f'{self.name}_0'
                                               )
        self.tanh_out.build(input_shape=input_shape)
        # self._trainable_weights = self.tanh_out.trainable_weights
        self.sigm_out = tf.keras.layers.Conv1D(filters=self.n_filters,
                                               kernel_size=2,
                                               dilation_rate=self.dilation_rate,
                                               activation='sigmoid',
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               padding='causal',
                                               name=f'{self.name}_1'
                                               )
        self.sigm_out.build(input_shape=input_shape)
        # self._trainable_weights.extends(self.sigm_out.trainable_weights)
        # self.mul_out = tf.multiply(tanh_out, sigm_out)
        self.residual_out = tf.keras.layers.Conv1D(filters=self.n_filters,
                                                   kernel_size=1,
                                                   padding='same',
                                                   use_bias=True,
                                                   kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                                   name=f'{self.name}_2',
                                                   )
        self.residual_out.build(input_shape=input_shape)
        # self._trainable_weights.extends(self.residual_out.trainable_weights)
        self.skip_out = tf.keras.layers.Conv1D(filters=self.n_filters,
                                               kernel_size=1,
                                               padding='same',
                                               use_bias=True,
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               name=f'{self.name}_3',
                                               )
        self.skip_out.build(input_shape=input_shape)
        self._trainable_weights = self.tanh_out.trainable_weights + self.sigm_out.trainable_weights + \
                                  self.skip_out.trainable_weights + self.skip_out.trainable_weights

    def call(self, x):
        tanh_out = self.tanh_out(x)
        sigm_out = self.sigm_out(x)

        mult = tanh_out * sigm_out

        residual_out = self.residual_out(mult)

        skip_out = self.skip_out(mult)
        # residual_2_out = x
        residual_2_out = x + residual_out
        return residual_2_out, skip_out


class Wavenet(tf.keras.Model):
    def __init__(self, n_stacks=5, dilation_depth=4, n_filters=32, use_skip_connections=True):
        super(Wavenet, self).__init__(name='Wavenet')
        self.n_stacks = n_stacks
        self.n_filters = n_filters
        self.dilation_depth = dilation_depth
        self.use_skip_connections = use_skip_connections
        res_block = []
        self.conv1 = tf.keras.layers.Conv1D(kernel_size=2, filters=n_filters, padding='causal')
        for s in range(n_stacks):
            res_block.append([])
            for d in range(dilation_depth + 1):
                res_block[s].append(ResidualBlock(dilation_rate=2 ** d, n_filters=n_filters, name=f'{s}_{d}'))
        self.res_block = res_block
        self.conv_out = tf.keras.layers.Conv1D(filters=1, kernel_size=1, padding='same',
                                               kernel_regularizer=tf.keras.regularizers.l2(0.01),
                                               activation='relu')
        self.add_n = tf.add_n
        self.relu = tf.nn.relu
        # self.metrics = [tf.keras.metrics.MeanAbsoluteError(name='mae')]

    def call(self, x):
        y = self.conv1(x)
        skip_connections = []
        for s in range(self.n_stacks):
            for d in range(self.dilation_depth + 1):
                y, skip_out = self.res_block[s][d](y)
                skip_connections += [skip_out]
        if self.use_skip_connections:
            y = tf.math.add_n(skip_connections)
        y = tf.keras.activations.relu(y)
        y = self.conv_out(y)
        return y

    # @tf.function
    # def train_loop(self, features, targets, eval_features, eval_targets, batch_size=64, epochs=100):
    #     dataset = self.make_dataset(features, targets, batch_size=batch_size, epochs=epochs)
    #     i = 0
    #     for x, y in dataset:
    #         with tf.GradientTape() as tape:
    #             predictions = self.call(features)
    #             loss = self.loss(targets, predictions)
    #         gradients = tape.gradient(loss, self.trainable_variables)
    #         self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))
    #         # self.train_loss(loss)
    #         i += 1
    #         if features.shape[0] % i == 0:
    #             self.eval(eval_features, eval_targets)

    def make_dataset(self, features, targets, batch_size, epochs=100):
        ds = tf.data.Dataset.from_tensor_slices((features, targets))\
            .batch(batch_size=batch_size, drop_remainder=True)\
            .shuffle(100)
        return ds
